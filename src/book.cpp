#include "book.h"


Book::Book(std::string titulo, std::string autor, std::string editora, std::string isbn):
  titulo_(titulo) , autor_(autor), editora_(editora), isbn_(isbn)
{
    // EM BRANCO
}
/************************************************************
   Getters
************************************************************/
std::string Book::getTitulo() const {
    return titulo_;
}
std::string Book::getAutor() const {
    return autor_;
}
std::string Book::getEditora() const {
    return editora_;
}
std::string Book::getISBN() const {
    return isbn_;
}

void Book::AdicionarColecao(const Colecao& valor) {
  colecao_.push_back(valor);
}

const std::vector<Colecao>& Book::colecao() const {
  return colecao_;
}

