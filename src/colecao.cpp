
#include "colecao.h"

Colecao::Colecao(std::string codigo, std::string nome) 
: codigo_(codigo), nome_(nome) {}

std::string Colecao::codigo() const {
  return codigo_;
}

std::string Colecao::nome() const {
  return nome_;
}
