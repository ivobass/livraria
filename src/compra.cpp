
#include "compra.h"

Compra::Compra(Colecao descricao_colecao, double valor_pago, time_t data)
    : descricao_colecao_(descricao_colecao), valor_pago_(valor_pago), data_(data) {}

Colecao Compra::descricao_colecao() const {
  return descricao_colecao_;
}

double Compra::valor_pago() const {
  return valor_pago_;
}

time_t Compra::data() const {
  return data_;
}

