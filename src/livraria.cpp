#include <fstream>
#include <iostream>
#include "livraria.h"


// void Livraria::addClientes(const Clientes& valor) {
//   clientes_.push_back(valor);
// }
// // uma destas duas fica definida com com push o com emplace
// void Livraria::addClientes(Clientes valor) {
//   clientes_.emplace_back(valor);
// }

Livraria::Livraria(std::string valor) : valor_(valor) {}

void Livraria::addClientes(const Cliente& valor) {
  clientes_.push_back(valor);
}

void Livraria::EmitirFatura(const Fatura& fatura) {
  faturas_.push_back(fatura);
}


void Livraria::RealizarCompra(const Compra& compra) {
  compras_.push_back(compra);
}

void Livraria::addBook(const Book& book) {
  livros_.push_back(book);
}

void Livraria::ListarColecoesAtribuidas(std::string nome_ficheiro) {
  std::ofstream ficheiro(nome_ficheiro);
  double total_gasto = 0;
  for (const Compra& compra : compras_) {
    ficheiro << compra.data() << " " << compra.descricao_colecao().codigo() << " "
             << compra.descricao_colecao().nome() << " " << compra.valor_pago()
             << std::endl;
    total_gasto += compra.valor_pago();
  }
  ficheiro << "Total gasto: " << total_gasto << std::endl;
  ficheiro.close();
}

std::string Livraria::IdentificarClienteMaiorGasto() {
  double maior_gasto = 0;
  std::string nif_cliente;
  for (const Cliente& cliente : clientes_) {
    if (cliente.getTotalGasto() > maior_gasto) {
      maior_gasto = cliente.getTotalGasto();
      nif_cliente = cliente.getnif();
    }
  }
  return nif_cliente;
}

