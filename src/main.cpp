#include <iostream>
#include "livraria.h"


int main() {
Livraria livraria("Livraria Ferdinando Persona");
// Adicionar livros comprados pelos clientes
livraria.addClientes(
Cliente(123456789, 987654321, "999999999", "João Silva", "Rua ABC, 123", 50.0));
livraria.addClientes(
Cliente(987654321, 123456789, "888888888", "Maria Oliveira", "Rua DEF, 456", 100.0));

// Adicionar faturas
livraria.EmitirFatura(Fatura("123456789", 100, time(nullptr)));
livraria.EmitirFatura(Fatura("987654321", 50, time(nullptr)));

  // Adicionar compras
livraria.RealizarCompra(Compra(Colecao("p1", "romanticos"), 20, time(nullptr)));
livraria.RealizarCompra(Compra(Colecao("p2", "terror"), 30, time(nullptr)));

// livraria.ListarColecoesAtribuidas();
// // Adicionar coleções
// colecoes.push_back(Colecao("O Senhor dos Anéis", "J.R.R. Tolkien", "Editora Martin Claret"));

// // Adicionar clientes
// clientes.push_back(Clientes(123456789, 987654321, "999999999", "João Silva", "Rua ABC, 123", 50.0));
// clientes.push_back(Clientes(987654321, 123456789, "888888888", "Maria Oliveira", "Rua DEF, 456", 100.0));

// Gravar livros comprados pelos clientes em ficheiro
livraria.ListarColecoesAtribuidas("livros.txt");

// // Identificar qual a coleção que mais livros vendeu
// livraria.identificarColecaoMaisVendida(colecoes);

return 0;
}

//   OficinaMecanica oficina("Oficina Mecânica de Francisco");

//   // Adicionar clientes
//   oficina.AdicionarCliente(
//       Cliente("123456789", "123456789", "987654321", "João da Silva", "Rua das Flores, nº 10", "Ford", "11-22-33"));
//   oficina.AdicionarCliente(
//       Cliente("987654321", "987654321", "123456789", "Maria Santos", "Rua dos Jardins, nº 15", "Fiat", "44-55-66"));

//   // Adicionar faturas
//   oficina.EmitirFatura(Fatura("123456789", 100, time(nullptr)));
//   oficina.EmitirFatura(Fatura("987654321", 50, time(nullptr)));

//   // Adicionar compras
//   oficina.RealizarCompra(Compra(Peca("p1", "Pneu"), 20, time(nullptr)));
//   oficina.RealizarCompra(Compra(Peca("p2", "Amortecedor"), 30, time(nullptr)));

//   // Adicionar carros
//   Carro carro1("Ford", 2010, 1500);
//   carro1.AdicionarPeca(Peca("p1", "Pneu"));
//   carro1.AdicionarPeca(Peca("p2", "Amortecedor"));
//   oficina.AdicionarCarro(carro1);

//   Carro carro2("Fiat", 2012, 2000);
//   carro2.AdicionarPeca(Peca("p3", "Freio"));
//   oficina.AdicionarCarro(carro2);

//   // Listar peças compradas e o total
//   oficina.ListarPecasCompradas("pecas_compradas.txt");

//   // Identificar cliente com maior gasto em reparações
//   std::cout << "Cliente com maior gasto: ";

//   std::cout << oficina.IdentificarClienteMaiorGasto() << std::endl;