#include "cliente.h"
/************************************************************
    Construtor principal
************************************************************/
Cliente::Cliente(int cc, int nif, std::string telefone, std::string nome, std::string morada, float totalGasto)
    :cc_(cc), nif_(nif), telefone_(telefone), nome_(nome), morada_(morada), totalgasto_(0)
  {   // construtor vazio
}
/************************************************************
   Getters
************************************************************/
int Cliente::getcc() const{
    return cc_;
}
int Cliente::getnif() const{
    return nif_;
}
std::string Cliente::getTelefone() const{
    return telefone_;
}
std::string Cliente::getNome() const{
    return nome_;
}
std::string Cliente::getMorada() const{
    return morada_;
}
float Cliente::getTotalGasto() const{
    return totalgasto_;
}

// void Clientes::imprimeClientes (){
//       std::cout << cc_ << "\t"<< nif_ << "\t" << telefone_
//             << "\t" << nome_ << "\t" << morada_
//             << "\t" << totalgasto_ << std::endl;
// }

/************************************************************
   Setters
************************************************************/

void Cliente::AdicionarGasto(double valor) {
  totalgasto_ += valor;
}