#pragma once
#include <string>
#include <vector>
#include "cliente.h"
#include "fatura.h"
#include "compra.h"
#include "book.h"

/************************************************************
    Definição da Clase Livraria
************************************************************/
class Livraria {
  /************************************************************
      Construtor principal
  ************************************************************/
public:
  Livraria(std::string valor);

//Getters
void addClientes(const Cliente& valor);
void EmitirFatura(const Fatura& fatura);
void RealizarCompra(const Compra& compra);
void addBook(const Book& book);
void ListarColecoesAtribuidas(std::string nome_ficheiro);
std::string IdentificarClienteMaiorGasto(); 
private:
  std::string valor_;
  std::vector<Cliente> clientes_;
  std::vector<Fatura> faturas_;
  std::vector<Compra> compras_;
  std::vector<Book> livros_;


};

