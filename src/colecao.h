#pragma once
#include <string>

class Colecao {
 public:
  Colecao(std::string codigo, std::string nome);
  std::string codigo() const;
  std::string nome() const;

 private:
  std::string codigo_;
  std::string nome_;
};


