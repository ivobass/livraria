/*
** UC: 21093 - Programação por Objectos - 03
** e-fólio A  2022-23 (efolioa.cpp)
**
** Aluno: 2100927 - Ivo Baptista
** Name        : EfolioA.cpp
** Author      : Ivo Baptista
** Version     : 1.0
** Copyright   : Your copyright notice
** Description : Atividades Imersivas in C++, Ansi-style
** ===========================================================================
*/
#pragma once
#include <string>
/************************************************************
    Definição da Clase Atividade
************************************************************/
class Cliente{
  /************************************************************
      Construtor principal
  ************************************************************/
public:
 Cliente(int cc, int nif, std::string telefone, std::string nome, std::string morada, float totalGasto);

  // getters
  int getcc() const;
  int getnif() const;
  std::string getTelefone() const;
  std::string getNome() const;
  std::string getMorada() const;
  float getTotalGasto() const;
  // Funções para imprimir na tela
  void imprimeClientes();
 
  // setters
  void addClientes(Cliente Valor);
  void AdicionarGasto(double valor);
  /************************************************************
      Variáveis privadas
  ************************************************************/
private:
  int cc_;
  int nif_;
  std::string telefone_;
  std::string nome_;
  std::string morada_;
  float totalgasto_;
};

