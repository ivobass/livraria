#pragma once
#include "colecao.h"
#include <ctime>

class Compra {
 public:
  Compra(Colecao descricao_colecao, double valor_pago, time_t data);
  Colecao descricao_colecao() const;
  double valor_pago() const;
  time_t data() const;

 private:
  Colecao descricao_colecao_;
  double valor_pago_;
  time_t data_;
};


