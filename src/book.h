#pragma once
#include <string>
#include <vector>
#include "colecao.h"

class Book {

public:
Book(std::string titulo, std::string autor, std::string editora, std::string isbn);

/************************************************************
   Getters
************************************************************/

std::string getTitulo() const;
std::string getAutor() const;
std::string getEditora() const;
std::string getISBN() const;
std::string getColecao() const;
void AdicionarColecao(const Colecao& valor);
const std::vector<Colecao>& colecao() const;
private:
  std::string titulo_;
  std::string autor_;
  std::string editora_;
  std::string isbn_;
  std::vector<Colecao>colecao_;
};




